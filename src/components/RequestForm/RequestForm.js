import React,{Component} from 'react'

import Input from '../Input/Input';

class RequestForm extends Component{

  state = {
    info : "Compila il form dei dati anagrafici del richiedente" +
      " per continuare con la prenotazione.",
    orderForm: {
      posizione: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'Militare in Servizio', displayValue: 'Militare in Servizio'},
            {value: 'Militare in Quiescenza', displayValue: 'Militare in Quiescenza'},
            {value: 'Personale Civile A.D.', displayValue: 'Personale Civile A.D.'},
            {value: 'Personale Estraneo A.D.', displayValue: 'Personale Estraneo A.D.'},
            {value: 'Associazione F.A.', displayValue: 'Associazione F.A.'},
            {value: 'Delegazione Straniera', displayValue: 'Delegazione Straniera'},
            {value: 'Militare altre F.A.', displayValue: 'Militare altre F.A.'},
            {value: 'Familiari Dipendenti A.D.', displayValue: 'Familiari Dipendenti A.D.'}
          ],
          label: 'Posizione*'
        },
        value: '',
        advise: '*Dato Obbligatorio',
        validation: {
          required: true
        },
        valid: false
      },
      cartaEsercito: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'N.EI - C820CE5 scadenza 06/08/2019',
          label: 'Carta Esercito'
        },
        value : '',
        advise: '',
        validation: {
          required: false
        },
        valid: false
      },
      nome: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Roberto',
          label: 'Nome*'
        },
        value: '',
        advise: '*Dato Obbligatorio',
        validation: {
          required: true
        },
        valid: false
      },
      cognome: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Bianchi',
          label: 'Cognome*'
        },
        value: '',
        advise: '*Dato Obbligatorio',
        validation: {
          required: true
        },
        valid: false
      },
      gradoQualifica: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'Primo Luogotenente', displayValue: 'Primo Luogotenente'},
            {value: 'Tenente Colonnello', displayValue: 'Tenente Colonnello'},
            {value: 'Generale', displayValue: 'Generale'},
            {value: 'Colonnello', displayValue: 'Colonnello'},
          ],
          label: 'Grado / Qualifica*'
        },
        value: '',
        advise: '*Dato Obbligatorio',
        validation: {
          required: true
        },
        valid: false
      },
      enteDiServizio: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Comando C4 Esercito',
          label: 'Ente di servizio'
        },
        value: '',
        advise: '',
        validation: {
          required: false
        },
        valid: false
      },
      indTelegrafico: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Inserisci un indirizzo telegrafico',
          label: 'Indirizzo telegrafico'
        },
        value: '',
        advise: '',
        validation: {
          required: false
        },
        valid: false
      },
      telMilitare: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Inserisci il numero di telefono militare',
          label: 'Telefono Militare'
        },
        value: '',
        advise: '',
        validation: {
          required: false
        },
        valid: false
      },
      telCivile: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Inserisci il numero di telefono civile',
          label: 'Telefono Civile'
        },
        value: '',
        advise: '',
        validation: {
          required: false
        },
        valid: false
      },
      cellulare: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: '339 5678 89 80',
          label: 'Cellulare'
        },
        value: '',
        advise: '',
        validation: {
          required: false
        },
        valid: false
      },
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'roberto.bianchi@email.com',
          label: 'Email*'
        },
        value: '',
        advise: '*Dato Obbligatorio',
        validation: {
          required: true
        },
        valid: false
      },
      motivoPrenotazione: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'Protezione Sociale', displayValue: 'Protezione Sociale'},
            {value: 'Servizio', displayValue: 'Servizio'},
            {value: 'Visite Istituzionali', displayValue: 'Visite Istituzionali'},
          ],
          label: 'Motivo della prenotazione*'
        },
        value: '',
        advise: '*Dato Obbligatorio',
        validation: {
          required: true
        },
        valid: false
      },
    },
    formIsValid: false
  };

  checkValidity(value, rules) {
    let isValid = true;
    if (!rules) {
      return true;
    }
    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }
    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid
    }
    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid
    }
    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid
    }
    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid
    }
    return isValid;
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedForm = {
      ...this.state.orderForm
    };
    const updatedFormElement = {
      ...updatedForm[inputIdentifier]
    };
    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value,
                                                  updatedFormElement.validation);
    updatedForm[inputIdentifier] = updatedFormElement;
    let formIsValid = true;
    for (let inputIdentifier in updatedForm) {
      formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
    }
    this.setState({orderForm: updatedForm, formIsValid: formIsValid});
  };

  onSubmit = (event) => {
    event.preventDefault();
    const formData = {};
    for (let elementIdentifier in this.state.orderForm) {
      formData[elementIdentifier] = this.state.orderForm[elementIdentifier].value;
    }
    if(!this.state.formIsValid) {
      alert("Form not valid" + formData);
    }
      alert("Prenotazione effettuata: "+formData);
  };

  ifRequired = (boolean) => {
    let css = {};
    if(boolean === true){
      css = {
        border: 0,
        borderRadius: 0,
        borderBottom: '1px solid red'
      }
    }
    if(boolean === false){
      css = {
        border: 0,
        borderRadius: 0,
        borderBottom: '1px solid #ccc'
        }
    }
    return css;
  };


  render() {

    const formElementsArray = [];
    for (let key in this.state.orderForm) {
      formElementsArray.push({
        id: key,
        config: this.state.orderForm[key]
      });
    }
    let form = (
      <form>
        {formElementsArray.map(formElement => (
          <div className="form-group float-left col-lg-6 md-12">
          <Input
            label={formElement.config.elementConfig.label}
            style={this.ifRequired(formElement.config.validation.required)}
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            changed={(event) => this.inputChangedHandler(event, formElement.id)}>
            <span className="small">{formElement.config.advise}</span>
          </Input>
          </div>
        ))}
      </form>
    );
    return (
      <div className="container-fluid">
        <h4>Verifica dati del richiedente</h4>
        <div className="row">
          <div className="col-6">
            <p>{this.state.info}</p>
          </div>
          <div className="col-6">
            <button className="btn btn-outline-info" onClick={this.onSubmit}>send</button>
          </div>
        </div>
        {form}
      </div>
    );
  }
}

export default RequestForm;