import React, { Component } from 'react';
import './App.css';

import RequestForm from './components/RequestForm/RequestForm'

class App extends Component {
  render() {
    return (
      <div className="App">
        <RequestForm/>
      </div>
    );
  }
}

export default App;
