import React from 'react';


const input = ( props ) => {
  let inputElement = null;

  switch ( props.elementType ) {
    case ( 'input' ):
      inputElement = <input
        style={props.style}
        className="form-control"
        {...props.elementConfig}
        value={props.value}
        onChange={props.changed}/>;
      break;
    case ( 'select' ):
      inputElement = (
        <select
          style={props.style}
          className="custom-select"
          value={props.value}
          onChange={props.changed}>
          {props.elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = <input
        style={props.style}
        {...props.elementConfig}
        value={props.value}
        onChange={props.changed} />;
  }

  const style = {
    float: 'right'
  };

  return (
    <div className={style}>
      <label className="small"><strong>{props.label}</strong></label>
      {inputElement}
    </div>
  );

};

export default input;